
from conans import ConanFile, CMake, tools
from conans.tools import os_info, SystemPackageTool
from pathlib import Path
import os
import shutil

homedir = Path.home()
conan_dir = os.path.join(homedir, ".conan")
conan_bin_dir = os.path.join(conan_dir, "bin")
if not os.path.exists(conan_bin_dir):
    os.mkdir(conan_bin_dir)


class sdl_helloConan(ConanFile):
    name = "sdl_hello"
    version = "0.0.1"
    license = "LGPL"
    author = "kaiyin keenzhong@qq.com"
    url = "https://github.com/kindlychung/sdl_hello"
    description = "change_your_description"
    topics = ("cpp", )
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    requires = ("sdl2/2.0.9@bincrafters/stable",
                "sdl2_image/2.0.4@bincrafters/stable", "sdl2_ttf/2.0.14@bincrafters/stable")
    generators = "cmake"
    exports_sources = "src/%s/*" % name, "src/CMakeLists.txt", "src/*.cmake"

    def system_requirements(self):
        pack_list = None
        if os_info.linux_distro == "ubuntu":
            pack_list = []
        if pack_list:
            for p in pack_list:
                installer = SystemPackageTool()
                installer.install(p)

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")
        cmake.build()

    def package(self):
        self.copy("*", dst="bin", src="bin")

    def imports(self):
        self.copy("*", dst="include", src="include")
        self.copy("*", dst="bin", src="lib")

    def deploy(self):
        self.copy("*", src="bin", dst=conan_bin_dir)
