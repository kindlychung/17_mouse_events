#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "l_button_sprite.h"
// The mouse button
class LButton {
   public:
    // Initializes internal variables
    LButton();
    // Sets top left position
    void setPosition(int x, int y);
    // Handles mouse event
    void handleEvent(SDL_Event* e);
    // Shows button sprite
    void render();

   private:
    // Top left position
    SDL_Point mPosition;
    // Currently used global sprite
    LButtonSprite mCurrentSprite;
};
