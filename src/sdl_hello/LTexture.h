#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <string>
// Texture wrapper class
class LTexture {
   public:
    // Initializes variables
    LTexture(SDL_Texture* texture = NULL, int width = 0, int height = 0,
             SDL_Rect* clips_ = nullptr, size_t clips_len_ = 0)
        : mTexture(texture),
          mWidth(width),
          mHeight(height),
          clips(clips_),
          clips_len(clips_len_) {}

    // Deallocates memory
    ~LTexture();

    // Loads image at specified path
    bool loadFromFile(std::string path);
    bool loadFromRenderedText(std::string textureText, SDL_Color textColor);

    // Deallocates texture
    void free();

    // Set blending
    void setBlendMode(SDL_BlendMode blending);
    // Set alpha modulation
    void setAlpha(Uint8 alpha);
    void setColor(Uint8 red, Uint8 green, Uint8 blue);
    LTexture& setClips(SDL_Rect* clips) {
        this->clips = clips;
        return *this;
    }
    LTexture& setClipsLen(size_t n) {
        if (n < 0) {
            std::cerr << "negative number given as length of clips "
                         "array.";
            return *this;
        }
        this->clips_len = n;
        return *this;
    }
    // Renders texture at given point
    void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0,
                SDL_Point* center = NULL,
                SDL_RendererFlip flip = SDL_FLIP_NONE);

    void animate();

    // Gets image dimension
    int getWidth();
    int getHeight();

   private:
    // The actual hardware texture
    SDL_Texture* mTexture;
    SDL_Rect* clips;
    size_t clips_len;
    size_t clipIndex;

    // Image dimensions
    int mWidth;
    int mHeight;
};
