#pragma once
// Screen dimension constants
const int SCREEN_WIDTH = 620;
const int SCREEN_HEIGHT = 420;

// Button constants
const int BUTTON_WIDTH = 300;
const int BUTTON_HEIGHT = 200;
const int TOTAL_BUTTONS = 4;
