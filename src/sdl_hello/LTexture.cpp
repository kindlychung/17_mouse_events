#include "LTexture.h"
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include "constants.h"

extern SDL_Renderer* gRenderer;
extern SDL_Window* gWindow;
extern SDL_Renderer* gRenderer;
extern TTF_Font* gFont;

LTexture::~LTexture() {
    // Deallocate
    free();
}

void LTexture::setBlendMode(SDL_BlendMode blending) {
    // Set blending function
    SDL_SetTextureBlendMode(mTexture, blending);
}

void LTexture::setAlpha(Uint8 alpha) {
    // Modulate texture alpha
    SDL_SetTextureAlphaMod(mTexture, alpha);
}

void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue) {
    // Modulate texture
    SDL_SetTextureColorMod(mTexture, red, green, blue);
}

bool LTexture::loadFromRenderedText(std::string textureText,
                                    SDL_Color textColor) {
    // Get rid of preexisting texture
    free();
    // Render text surface
    SDL_Surface* textSurface =
        TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
    if (textSurface == NULL) {
        printf("Unable to render text surface! SDL_ttf Error: %s\n",
               TTF_GetError());
    } else {
        // Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
        if (mTexture == NULL) {
            printf(
                "Unable to create texture from rendered text! SDL Error: %s\n",
                SDL_GetError());
        } else {
            // Get image dimensions
            mWidth = textSurface->w;
            mHeight = textSurface->h;
        }

        // Get rid of old surface
        SDL_FreeSurface(textSurface);
    }

    // Return success
    return mTexture != NULL;
}

bool LTexture::loadFromFile(std::string path) {
    // Get rid of preexisting texture
    free();

    // The final texture
    SDL_Texture* newTexture = NULL;

    // Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if (loadedSurface == NULL) {
        printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(),
               IMG_GetError());
    } else {
        // Color key image, all #00ffff pixels are considered background and
        // will become transparent
        SDL_SetColorKey(loadedSurface, SDL_TRUE,
                        SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

        // Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
        if (newTexture == NULL) {
            printf("Unable to create texture from %s! SDL Error: %s\n",
                   path.c_str(), SDL_GetError());
        } else {
            // Get image dimensions
            mWidth = loadedSurface->w;
            mHeight = loadedSurface->h;
        }

        // Get rid of old loaded surface
        SDL_FreeSurface(loadedSurface);
    }

    // Return success
    mTexture = newTexture;
    return mTexture != NULL;
}

void LTexture::free() {
    // Free texture if it exists
    if (mTexture != NULL) {
        SDL_DestroyTexture(mTexture);
        mTexture = NULL;
        mWidth = 0;
        mHeight = 0;
    }
}

void LTexture::render(int x, int y, SDL_Rect* clip, double angle,
                      SDL_Point* center, SDL_RendererFlip flip) {
    // renderQuad is the coordinates and dimensions relative to screen
    SDL_Rect renderQuad = {x, y, mWidth, mHeight};
    // clip is the coordinates and dimensions relative to the texture
    if (clip != NULL) {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }
    // Render to screen
    SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center,
                     flip);
}

void LTexture::animate() {
    static const int N = 1500;
    static const int limit = clips_len * N;
    if (clips) {
        if (clipIndex >= limit) {
            clipIndex = 0;
        }
        // slow the animation down by a factor of N
        auto idx = clipIndex / N;
        const int x = (SCREEN_WIDTH - clips[idx].w) / 2;
        const int y = (SCREEN_HEIGHT - clips[idx].h) / 2;
        render(x, y, &clips[idx]);
        clipIndex++;
    } else {
        render(0, 0, NULL);
    }
}

int LTexture::getWidth() { return mWidth; }

int LTexture::getHeight() { return mHeight; }
